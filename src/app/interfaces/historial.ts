export interface Historial {
  
    nombre: string,
    rut: string,
    banco: string,
    tipocuenta: string,
    monto: string

}
