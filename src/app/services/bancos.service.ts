import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { from, Observable, of } from 'rxjs';
import { Bancos } from '../interfaces/bancos';
import { environment } from '../../environments/environment.prod';

const base_url = environment.base_url;


@Injectable({
  providedIn: 'root'
})
export class BancosService {

  constructor(
    private http: HttpClient, 
    private router: Router,
    private ngZone: NgZone 
  ) { }

  test(){
		return "Hola Mundo desde un servicio!!";
	} 
  


    bancos():Observable<Bancos[]> {
    
    
    return this.http.get<Bancos[]>(`${base_url}/banco`).pipe( 
      tap((res: any) => {
        
        
        sessionStorage.setItem('info', JSON.stringify(res) );     
                  

      }
       ));
    

    

  }

}
