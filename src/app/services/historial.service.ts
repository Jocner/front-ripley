import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { from, Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { Historial } from '../interfaces/historial';


const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class HistorialService {

  constructor(
    private http: HttpClient, 
    private router: Router,
    private ngZone: NgZone
  ) { }


  cartola():Observable<Historial> {
    
    //let listaBancos = 'https://bast.dev/api/banks.php';  
    return this.http.get<Historial>(`${base_url}/historial`).pipe( 
      tap((res: any) => {
        
        // localStorage.setItem('data', res );
        console.log("historia",res )
        sessionStorage.setItem('transaciones', JSON.stringify(res.cartola) );              

      }
       ));
    

    // return this.http.get<Bancos[]>(listaBancos);

  }


}
