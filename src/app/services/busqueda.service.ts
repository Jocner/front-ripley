import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { from, Observable, of } from 'rxjs';
import { Busqueda } from '../interfaces/busqueda';
import { environment } from '../../environments/environment.prod';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class BusquedaService {

  constructor(
    private http: HttpClient, 
    private router: Router,
    private ngZone: NgZone 
  ) { }


  clientes():Observable<Busqueda[]> {
    
    
    return this.http.get<Busqueda[]>(`${base_url}/busqueda`).pipe( 
      tap((res: any) => {
        
     
        sessionStorage.setItem('cliente', JSON.stringify(res.cliente) );     
                  

      }
       ));

    }     
}
