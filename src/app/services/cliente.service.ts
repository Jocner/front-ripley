import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { Registro } from '../interfaces/registro';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(
    private http: HttpClient, 
    private router: Router,
    private ngZone: NgZone,

  ) { }

  test(){
		return "Hola Mundo desde un servicio!!";
	}
  
  crearUsuario( formData: Registro) {

  
    
    return this.http.post(`${ base_url }/registro`, formData )
              .pipe(
                tap( (resp: any) => {
                  console.log('desde servicio', JSON.stringify(resp));
                  localStorage.setItem('data', JSON.stringify(resp))
                })
              )

  }

}
