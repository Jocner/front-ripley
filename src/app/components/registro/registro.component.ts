import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { BancosService } from '../../services/bancos.service';
import { ClienteService } from '../../services/cliente.service';
import { HistorialService } from '../../services/historial.service';
import { BusquedaService } from '../../services/busqueda.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers: [ClienteService]
})
export class RegistroComponent {

  public formSubmitted = false;

 // registroForm: any;
  rut: string = '';
  nombre: string = '';
  email: string = '';
  telefono: string = '';
  bancodestino: string = '';
  tipocuenta: string = '';
  numerocuenta: string = '' ;
  resul: string = '';
  array: string = '';

  result1 = sessionStorage.getItem('transaciones');
  info2 = JSON.parse(this.result1);

  result = sessionStorage.getItem('info');
  info = JSON.parse(this.result);

  resul3 = sessionStorage.getItem('cliente');
  info3 = JSON.parse(this.resul3);
  

  public registroForm  = this.fb.group({
    rut: ['', [ Validators.required, Validators.maxLength(12) ] ],
    nombre: ['', Validators.required ],
    email: ['', [ Validators.required, Validators.email ] ],
    telefono: ['', Validators.required],
    bancodestino: ['', Validators.required],
    tipocuenta: ['', Validators.required],
    numerocuenta: ['', Validators.required]
  });


  constructor(
     private fb: FormBuilder,
     public bancosServices: BancosService,
     public clienteService: ClienteService,
     public historialService: HistorialService,
     public busquedaServicio: BusquedaService,
     private router: Router 
  ) { }

  ngOnInit(): void {

    this.bancosServices.bancos().subscribe(
      res => {

        console.log("servicio bancos", res);

      })
      

    this.historialService.cartola().subscribe(
      data => {
         console.log("principal", data);
     
        
     
        
          console.log("resul local", this.info);
  
  
          
  
      })

      this.busquedaServicio.clientes().subscribe(
        res => {
 
         
          console.log("cliente", this.info3);
         
 
     }, (err) => {
       // Si sucede un error
       console.log(" detalle error",err);
     })
 
  }


  
  

  crearUsuario() {
    this.formSubmitted = true;
    console.log( this.registroForm );


    // Realizar el posteo
    this.clienteService.crearUsuario( this.registroForm.value)
        .subscribe( resp => {

          if ( this.registroForm.invalid ) {
            return;
          }

          //  Swal.fire('Felicitaciones', 'Registrado' );

          if(resp){
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Registro exitoso',
              showConfirmButton: false,
              timer: 1300
            })

          }
          
          // Navegar al Dashboard
          this.router.navigate(['/home']);

        }, (err) => {
          // Si sucede un error
          Swal.fire('Error', err.error.msg, 'error' );
        });


  }


}
